import InputError from "@/Components/InputError";
import InputLabel from "@/Components/InputLabel";
import SelectInput from "@/Components/SelectInput";
import TextAreaInput from "@/Components/TextAreaInput";
import TextInput from "@/Components/TextInput";
import VariationGroup from "@/Components/VariationGroup";
import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import { useEffect, useState } from "react";

export default function Create({ auth, categories, sizes}) {

  const { data, setData, post, errors, reset } = useForm({
    image: "",
    name: "",
    taxe: "",
    description: "",
  });

  const [variationGroups, setVariationGroups] = useState([{ size: '1', color: 'red', price: 10000 }]);

  categories = categories || {};

  const handleClick = () => {
    setVariationGroups((prevGroups) => [...prevGroups, { size: 'XL', color: 'green', price: 50 }], () => {
      console.log('Variation groups updated:', variationGroups); // Optional for debugging
    });
  };

  return (
    <AuthenticatedLayout
      user={auth.user}
      header={
        <div className="flex justify-between items-center">
          <h2 className="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            Create new Product
          </h2>
        </div>
      }
    >
      <Head title="Products" />

      <div className="py-12">
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
            <form
              // onSubmit={onSubmit}
              className="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg"
            >
              <div>
                <InputLabel
                  htmlFor="product_image_path"
                  value="Product Image"
                />
                <TextInput
                  id="product_image_path"
                  type="file"
                  name="image"
                  className="mt-1 block w-full"
                  onChange={(e) => setData("image", e.target.files[0])}
                />
                <InputError message={errors.image} className="mt-2" />
              </div>
              <div className="mt-4">
                <InputLabel htmlFor="product_name" value="Name" />

                <TextInput
                  id="product_name"
                  type="text"
                  name="name"
                  value={data.name}
                  className="mt-1 block w-full"
                  isFocused={true}
                  onChange={(e) => setData("name", e.target.value)}
                />

                <InputError message={errors.name} className="mt-2" />
              </div>
              <div className="mt-4">
                <InputLabel htmlFor="product_taxe" value="Taxe" />

                <TextInput
                  id="product_taxe"
                  type="text"
                  name="taxe"
                  value={data.taxe}
                  className="mt-1 block w-full"
                  isFocused={true}
                  onChange={(e) => setData("taxe", e.target.value)}
                />

                <InputError message={errors.taxe} className="mt-2" />
              </div>
              <div className="mt-4">
                <InputLabel
                  htmlFor="product_description"
                  value="Product Description"
                />

                <TextAreaInput
                  id="product_description"
                  name="description"
                  value={data.description}
                  className="mt-1 block w-full"
                  onChange={(e) => setData("description", e.target.value)}
                />

                <InputError message={errors.description} className="mt-2" />
              </div>
            
              <div className="mt-4">
                <InputLabel htmlFor="product_categorie" value="Catégories" />

                <SelectInput
                  name="categories"
                  id="product_categorie"
                  className="mt-1 block w-full"
                  onChange={(e) => setData("categories", e.target.value)}
                >
                  <option value="">Select Status</option>
                  {categories.data.map((categorie) => (
                      <option value={categorie.id}>{categorie.name}</option>
                  ))}
                </SelectInput>

                <InputError message={errors.categories} className="mt-2" />
              </div>
              <div className="mt-4">
                <InputLabel htmlFor="product_size" value="Sizes" />

                <SelectInput
                  name="sizes"
                  id="product_size"
                  className="mt-1 block w-full"
                  onChange={(e) => setData("sizes", e.target.value)}
                >
                  <option value="">Select Size</option>
                  {sizes.data.map((size) => (
                      <option value={size.id}>{size.name}</option>
                  ))}
                </SelectInput>

                <InputError message={errors.sizes} className="mt-2" />
              </div>

              <div className="mt-4">
                  <button className="bg-blue-500 text-white px-4 py-2 rounded mt-4" onClick={handleClick}>
                    Add Variation Group
                  </button>
              </div>

              {variationGroups.map((group, index) => (
                  <VariationGroup
                    key={index}
                    id={index}
                    group={group}
                    onUpdate={(updatedGroup) => {
                      const updatedGroups = [...variationGroups];
                      updatedGroups[index] = updatedGroup;
                      setVariationGroups(updatedGroups);
                    }}
                    onDelete={() => {
                      const updatedGroups = [...variationGroups];
                      updatedGroups.splice(index, 1);
                      setVariationGroups(updatedGroups);
                    }}
                  />
                ))}

              <div className="mt-4 text-right">
                <Link
                  href={route("product.index")}
                  className="bg-gray-100 py-1 px-3 text-gray-800 rounded shadow transition-all hover:bg-gray-200 mr-2"
                >
                  Cancel
                </Link>
                <button className="bg-emerald-500 py-1 px-3 text-white rounded shadow transition-all hover:bg-emerald-600">
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </AuthenticatedLayout>
  );
}
