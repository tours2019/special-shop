import Pagination from '@/Components/Pagination';
import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, Link, router } from "@inertiajs/react";

export default function Index({ auth, products }) {
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">Produits</h2>}
        >
            <Head title="Products" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900 dark:text-gray-100">
                            <div className="grid grid-cols-1 gap-4 sm:grid-cols-2  lg:grid-cols-4">
                              {products.data.map((product) => (
                                <div
                                className="bg-white border border-[#fec71280] rounded-lg shadow ">
                                   <img
                                        src={product.image_path}
                                        alt=""
                                        className="w-full h-64 object-cover"
                                    />
                                   <div className="px-4 py-4">
                               
                                       <div className="flex items-center space-x-2 pb-3">
                                           <div className="flex-shrink-0 bg-[#D9D9D9] w-[40px] h-[40px] rounded-full flex items-center justify-center">
                                            
                                           </div>
                                           <div className="flex-1 min-w-0">
                                               <span className="text-[15px] font-[700] text-gray-900 quicksand truncate dark:text-white max-w-[90%] block">
                                                   
                                               </span>
                                           </div>
                                           
                                       </div>
                               
                                       <div className="mb-3">
                                        <Link
                                            href={route("product.show", product.id)}
                                            className="block "
                                        >
                                             <h5
                                                   class="truncate  text-[17px] font-bold tracking-tight text-gray-900 dark:text-white max-w-[95%]">
                                                    {product.name}
                                               </h5>
                                        </Link>
                                    
                                       </div>
                               
                                       <p className="mb-3 font-normal text-gray-700 dark:text-gray-400 h-[80px] overflow-hidden">
                                            {product.description}
                                       </p>
                               
                                       <div className="w-full pt-3 text-left">

                                            <Link
                                                href={route("product.show", product.id)}
                                                className="widget-link py-2 px-6 w-[150px]"
                                            >
                                               <span className="text-[#000] text-[18px] !font-[600] btn-no-loader quicksand">
                                                   Lire plus
                                               </span>
                                            </Link>
                                           
                                       </div>
                               
                                   </div>
                               </div>
                              ))}
                            </div>
                        </div>

                        <Pagination links={products.meta.links} />

                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}