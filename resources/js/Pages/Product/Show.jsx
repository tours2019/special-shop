import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link } from "@inertiajs/react";

export default function Show({ auth, product }) {
  return (
    <AuthenticatedLayout
      user={auth.user}
      header={
        <div className="flex items-center justify-between">
          <h2 className="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {`Product "${product.name}"`}
          </h2>
        </div>
      }
    >
      <Head title={`Project "${product.data.name}"`} />
      <div className="py-12">

        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
          <div className="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
            <div>
              <img
                src={product.data.image_path}
                alt=""
                className="w-full h-64 object-cover"
              />
            </div>
            <div className="p-6 text-gray-900 dark:text-gray-100">
              <div className="grid gap-1 grid-cols-2 mt-2">
                <div>
                  <div>
                    <label className="font-bold text-lg">Product</label>
                    <p className="mt-1">{product.data.reference}</p>
                  </div>
                  <div className="mt-4">
                    <label className="font-bold text-lg">Product Name</label>
                    <p className="mt-1">{product.data.name}</p>
                  </div>

                  
                {/* <div>
                  <div>
                    <label className="font-bold text-lg">Brand</label>
                    <p className="mt-1">{product.variations.brand}</p>
                  </div>
                  <div className="mt-4">
                    <label className="font-bold text-lg">Color</label>
                    <p className="mt-1">{product.variations.color.name}</p>
                  </div>
                  <div className="mt-4">
                    <label className="font-bold text-lg">Size</label>
                    <p className="mt-1">{product.variations.size.name}</p>
                  </div>
                </div> */}
              </div>

              <div className="mt-4">
                <label className="font-bold text-lg">Product Description</label>
                <p className="mt-1">{product.data.description}</p>
              </div>
            </div>
          </div>
          <div className="flex flex-row justify-between mx-20 mb-10">
            <p>Quantity in stock: </p>
          <button className="bg-emerald-500 py-1 px-3 text-white rounded shadow transition-all hover:bg-emerald-600 text-center">
                  Add to cart
                </button>
          </div>
          
        </div>

      </div>
      </div>

    </AuthenticatedLayout>
  );
}
