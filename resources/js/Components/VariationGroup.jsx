import InputLabel from "./InputLabel";
import NumberInput from "./NumberInput";
import TextInput from "./TextInput";

export default function VariationGroup({ id, group, onUpdate, onDelete }) {
    return (
      <div className="mt-4 border rounded-md p-4">
        <div className="flex justify-between">
          <h3>Variation {id + 1}</h3>
          <button className="bg-red-500 text-white px-2 py-1 rounded" onClick={onDelete}>
            Delete
          </button>
        </div>
  
        <div className="mt-2">
          <InputLabel htmlFor={`size_${id}`} value="Size" />
          <TextInput
            type="text"
            id={`size_${id}`}
            className="mt-1 block w-full"
            value={group.size}
            onChange={(e) => onUpdate({ ...group, size: e.target.value })}
          />
        </div>
  
        <div className="mt-2">
          <InputLabel htmlFor={`color_${id}`} value="Color" />
          <TextInput
            type="text"
            id={`color_${id}`}
            className="mt-1 block w-full"
            value={group.color}
            onChange={(e) => onUpdate({ ...group, color: e.target.value })}
          />
        </div>
  
        <div className="mt-2">
          <InputLabel htmlFor={`price_${id}`} value="Price" />
          <NumberInput
            type="number"
            id={`price_${id}`}
            className="mt-1 block w-full"
            value={group.price}
            onChange={(e) => onUpdate({ ...group, price: e.target.value })}
          />
        </div>
      </div>
    );
  };
  