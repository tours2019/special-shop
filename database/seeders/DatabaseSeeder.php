<?php

namespace Database\Seeders;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Categorie;
use App\Models\CategorieProduct;
use App\Models\Comment;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\ProductVariation;
use App\Models\Stock;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        User::factory()->create([
            'name' => 'Joel',
            'email' => 'joel@example.com',
            'password' => bcrypt('123.321A'),
            'email_verified_at' => time()
        ]);

        User::factory()->create([
            'name' => 'sam',
            'email' => 'sam@example.com',
            'password' => bcrypt('123.321A'),
            'email_verified_at' => time()
        ]);

        User::factory()->create([
            'name' => 'franc',
            'email' => 'franc@example.com',
            'password' => bcrypt('123.321A'),
            'email_verified_at' => time()
        ]);

        Product::factory()
        ->count(30)
        ->create();

        ProductVariation::factory()
        ->count(50)
        ->create();

        ProductSize::factory()
        ->count(4)
        ->create();

        Categorie::factory()
        ->count(5)
        ->create();

        CategorieProduct::factory()
        ->count(30)
        ->create();

        ProductSize::factory()
        ->count(4)
        ->create();

        Cart::factory()
        ->count(10)
        ->create();

        CartItem::factory()
        ->count(10)
        ->create();

        Comment::factory()
        ->count(10)
        ->create();

        Stock::factory()
        ->count(100)
        ->create();
    }
}
