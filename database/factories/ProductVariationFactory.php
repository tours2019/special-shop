<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductVariation>
 */
class ProductVariationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'price' => fake()->numberBetween(0,15),
            'color' => fake()->randomElement(['red','blue','black','green']),
            'brand' => fake()->sentence(),
            'product_id' => fake()->numberBetween(1,30),
            'product_size_id' => fake()->randomElement([1,2,3]),
            'image_path' => fake()->imageUrl(),
            'created_at' => time(),
            'updated_at' => time()
        ];
    }
}
