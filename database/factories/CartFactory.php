<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cart>
 */
class CartFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'sub_total' => fake()->numberBetween(1000, 10000),
            'total' =>  fake()->numberBetween(1500, 15000),
            'expired_at' => fake()->dateTime(),
            'user_id' => fake()->randomElement([1,2,3]),
            'created_at' => time(),
            'updated_at' => time()
        ];
    }
}


