<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comment>
 */
class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'content' => fake()->realText(),
            'product_id' => fake()->numberBetween(1,30),
            'user_id' => fake()->randomElement([1,2,3]),
            'created_at' => time(),
            'updated_at' => time()
        ];
    }
}
