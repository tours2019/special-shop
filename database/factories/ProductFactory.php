<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->sentence(),
            'description' => fake()->realText(),
            'reference' => fake()->numerify('user-####'),
            'taxe' => '20%',
            'image_path' => fake()->imageUrl(),
            'created_at' => time(),
            'updated_at' => time()
        ];
    }
}
