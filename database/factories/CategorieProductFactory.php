<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CategorieProduct>
 */
class CategorieProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'categorie_id' => 1,
            'product_id' => fake()->numberBetween(1,30),
            'created_at' => time(),
            'updated_at' => time()
        ];
    }
}
