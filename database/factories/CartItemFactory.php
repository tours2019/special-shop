<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CartItem>
 */
class CartItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'unit_price' => fake()->numberBetween(1000, 10000),
            'quantity' =>  fake()->numberBetween(1, 10),
            'sub_total' => fake()->numberBetween(1500, 15000),
            'cart_id' => fake()->numberBetween([1,10]),
            'product_id' => fake()->numberBetween([1,30]),
            'created_at' => time(),
            'updated_at' => time()
        ];
    }
}


