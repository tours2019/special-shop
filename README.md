##Server Requirements

- PHP >= 8.2

## Installation

### Clone git project
'''
git clone https://gitlab.com/tours2019/special-shop.git

'''
### Composer
'''
cd special-shop
composer install
'''

### Database & Migration
config database connection in .env file
'''

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=special_shop
DB_USERNAME=root
DB_PASSWORD=

'''
Run this command in console
php artisan migrate --seed
npm install
'''

'''
### serve
'''
run application now

'''
php artisan serve

'''
'''
npm run dev

'''
