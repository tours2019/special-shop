<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ProductVariation extends Model
{
    use HasFactory;

     protected $fillable = ['colore', 'brand', 'price', 'product_id', 'product_size_id'];


    public function product()
    {
        return $this->belongsTo(Product::class);   
    }

    public function productSize(): HasMany
    {
        return $this->hasMany(ProductSize::class,'id');
    }

    public function stock(): HasOne
    {
        return $this->hasOne(Stock::class);
    }
    
}
