<?php

namespace App\Models;

use App\Models\Categorie;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'reference', 'description', 'taxe', 'image_path'];

    public function categories()
    {
        return $this->belongsToMany(Categorie::class);
    }

    public function variations()
    {
        return $this->hasMany(ProductVariation::class);
    }
}
