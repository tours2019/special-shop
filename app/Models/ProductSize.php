<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductSize extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'type', 'description', 'taxe', 'image_path'];

    public function variations() :BelongsTo
    {
        return $this->belongsTo(ProductVariation::class);   
    }
}
