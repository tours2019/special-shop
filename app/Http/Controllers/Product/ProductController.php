<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\CategorieResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductSizeResource;
use App\Models\Categorie;
use App\Models\ProductSize;
use App\Models\ProductVariation;
use Exception;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $products = Product::query()
            ->paginate(10)
            ->onEachSide(1);

        return inertia("Product/Index", [
            "products" => ProductResource::collection($products),
            'success' => session('success'),
        ]);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Categorie::all(); 
        $colors = ['Red', 'Green', 'Blue']; 
        $sizes = ProductSize::all();

        return inertia("Product/Create",[
            "categories" => CategorieResource::collection($categories),
            "sizes" => ProductSizeResource::collection($sizes)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProductRequest $request)
    {
        $data = $request->validated();

        try{

            /** @var $image \Illuminate\Http\UploadedFile */
            if ($data['image']) {
                $data['image'] = $data['image']->store('product/' . Str::random(), 'public');
            }

            // Creat product
            $product = Product::create($data);

            $product->categories()->attach($data['categories']);

            $productVariation = ProductVariation::create([
                'product_id' => $product->id,
                'color' => $data['color'],
                'price' => $data['price'],
                'brand' => $data['brand'],
            ]);

            $productVariation->productSize()->attach($data['sizes']);

            return to_route('product.index')
                ->with('success', 'Product was created');

        }catch (Exception $e) {

            // Handle the exception and display an appropriate error message

            $errorMessage = 'An error occurred while creating the product.';

            if ($e instanceof \Illuminate\Database\QueryException) {
                $errorMessage = 'Erreur de base de données : ' . $e->getMessage();
            }

            return to_route('product.index')
                ->with('error',  $errorMessage);
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        return inertia('Product/Show', [
            'product' => new ProductResource($product)]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        return inertia('Product/Edit', [
            'project' => new ProductResource($product),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $name = $product->name;
        $product->delete();
        if ($product->image_path) {
            Storage::disk('public')->deleteDirectory(dirname($product->image_path));
        }
        return to_route('product.index')
            ->with('success', "Product \"$name\" was deleted");
    }
}
