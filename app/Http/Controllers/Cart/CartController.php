<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Http\Requests\StoreCartRequest;
use App\Http\Requests\UpdateCartRequest;
use App\Http\Resources\CartRessource;
use App\Models\CartItem;
use App\Models\ProductVariation;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCartRequest $request)
    {
        $isSuccess = true;
        $productId = $request->input('product_id');
        $quantity = $request->input('quantity', 1); // Default quantity to 1

        $cart = auth()->user()->cart ?? new Cart; // Get or create cart for the user

        // Check if product already exists in cart
        $cartItem = $cart->items->where('product_id', $productId)->first();

        $product = ProductVariation::find($productId);
        $stock = $product->stock; 

        if (!$stock || $stock->quantity < $quantity) {
            // Handle insufficient stock error
            $isSuccess = false;
        }else{
            if ($cartItem) {
                // Update existing cart item quantity
                $cartItem->quantity += $quantity;
                $cartItem->save();
            } else {
                // Add new cart item
                $cartItem = new CartItem;
                $cartItem->cart_id = $cart->id;
                $cartItem->product_id = $productId;
                $cartItem->quantity = $quantity;
                $cartItem->save();
    
                // Save the cart if it's a new one
                if (!$cart->exists) {
                    $cart->save();
                }
            }

            $stock->quantity -= $quantity; //decrease quantity            
            $stock->save();

        }

        if($isSuccess){
            return to_route('product.index')
            ->with('success', 'Product added to cart');
        }else{
            return to_route('product.index')
            ->with('error', 'Rate of stock');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Cart $cart)
    {
        $cart = Cart::query()
            ->where('user_id', Auth::id())
        ;

        return inertia("Cart/Index", [
            "cart" => CartRessource::collection($cart),
            'success' => session('success')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCartRequest $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Cart $cart)
    {
        //
    }
}
