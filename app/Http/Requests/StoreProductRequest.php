<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "name" => ['required', 'max:255'],
            'image' => ['nullable', 'image'],
            "description" => ['nullable', 'string'],
            "taxe" => ['required', 'max:255'],
            "categories" => "required|array",
            "categories.*" => "required|numeric",
            "color" => ['nullable', 'max:55'],
            "brand" => ['required', 'max:55'],
            "price" => ['required', 'max:55'],
            "quantity" => "required|numeric",
            "sizes" => "required|array",
            "sizes.*" => "required|numeric",
        ];
    }
}
